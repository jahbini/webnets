<div class="TweetBoxHolder">
<p>TweetBox</p>
<div ID="TweetBox" class="$tweetBoxClass"  >
$TweetBox
<div ID="TweetBoxLegend"></div>
<div ID="TweetBoxError"></div>
<div id="LocalAction">
        <div class="localaction" id="action_copy">cp</div>
        <div class="localaction" id="action_retweet">rt</div>
        <div class="localaction" id="action_goaway">x</div>
        <div class="localaction" id="action_FF">ff</div>
        <div class="localaction" id="action_direct">DT</div>
        <div class="inaction"> </div>
        <div class="localaction" id="action_touser">@</div>
        <div class="localaction" id="action_append">ps</div>
        <div class="localaction" id="action_follow">F</div>
        <div class="localaction" id="action_unfollow">U</div>
        <div class="localaction" id="action_20">20</div>
</div>
</div>
</div>
